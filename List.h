#pragma once
#include <iostream>
#include <optional>
#include <functional>
#include <initializer_list>

using namespace std;

using TData = int;
 

enum class ErrorCode
{
	node_error,
	remove_error,
	insert_error,
	erase_error
};


//template <typename T> ��������
//������� ���� �� ����������� operator[]

class Node {

public:
	Node(TData data = 0, Node* next = nullptr, Node* prev = nullptr);

	TData data;
	Node* next;
	Node* prev;
private:
};

class List {
public:
	List();
	List(initializer_list<TData>);
	~List();

	bool empty();
	void push_back(TData data);
	void push_front(TData data);
	void pop_back();
	void pop_front();
	void clear();
	size_t size() const;
	optional<TData> at(size_t i) const;
	
	void each(function<void(Node&)> callback);
	void each(function<void(const Node&)> callback) const;
	void each(function<void(Node&, size_t)> callback);
	//void each(const List& object, function<void(Node&)> callback);
	
	void insert(size_t position, const TData& data);
	void erase(size_t position);
	void merge(const List& object);
	
	//void merge();

	List& operator=(List& object);
	void operator<<(const TData& a);

private:
	Node* head;
	Node* tail;
	size_t size_;

	void insertingBody(Node& iter, const TData& data, Node* newNode);
	void erasingBody(Node* iter);
	const char* errorDecoding(const ErrorCode& error);
	void rangeCheck(size_t position, const ErrorCode& error);
	Node* newNodeCreation(const TData& data);
};

ostream& operator<<(ostream& cout, const optional<TData>& data);
ostream& operator<<(ostream& cout, const List& object);
