#include "List.h"
#include <thread>

//empty-
//pop\push_back\front-
//size-
//clear-
//-----emplace_back\fron ?
//-----resize
//insert
//erase
//-----operator=
//-----erase_if
//-----merge

void launch() 
{
	//List list{};	  Go to constructor
	//List list = {}; Go to constructor
	//List list();	  Go to constructor
	//List list = ..; Go to constructor

	List list = { 4, 5, 1 }; // {} - init. list
	
	//List<int> list = { 4, 5, 1 };
	/*initializer_list< int > initList = { 4, 5, 1 };
	cout << initList.begin()[1] << endl;;*/
	cout << list;
	list.push_front(3);
	cout << list;
	list.insert(1, 7);
	cout << list;
	list.erase(1);
	cout << list;
	List list2 = { 5, 8, 9 };
	list.merge(list2);
	cout << list;
	/*list.push_back(10);
	list.push_back(15);
	list.push_back(20);
	cout << list;

	//cout << list.size();

	list.insert(2, 18);
	cout << list;
	//list.each([](Node& node) {
	//	cout << node.data << " ";
	//});

	list.erase(2);
	cout << list;

	List list2;
	list2 = list;
	cout << list2;

	list << 2;
	list << 4;
	cout << list;*/
}

int main() {
	cout << thread::hardware_concurrency() << endl;

	try
	{
		launch();
	}
	catch (const runtime_error& error){
		cout << "\n" << error.what() << endl;
	}

	return 0;
}