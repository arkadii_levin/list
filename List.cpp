#include "List.h"

Node::Node(TData data, Node* next, Node* prev) : 
	data(data), 
	next(next), 
	prev(prev) 
{}

List::List() : 
	head(nullptr), 
	tail(nullptr), 
	size_(0) 
{};
//               v
//   4   6   7   .
//   ^           ^
//   begin       end

List::List(initializer_list<TData> initList) {
	for (const TData* i = initList.begin(); i != initList.end(); i++)
	{
		push_back(*i);
	}
}

List::~List() {
	clear();
}

bool List::empty() {
	return head == nullptr && tail == nullptr;
}

const char* List::errorDecoding(const ErrorCode& error) {
	switch (error)
	{
	case ErrorCode::node_error:
		return "Node creation error";

	case ErrorCode::remove_error:
		return "Trying to remove element in an empty list";

	case ErrorCode::insert_error:
		return "Attempt to insert an element outside the list";

	case ErrorCode::erase_error:
		return "Attempt to erase an element outside the list";

	default:
		break;
	}
}

Node* List::newNodeCreation(const TData& data) {
	auto newNode = new Node(data);

	if (newNode == nullptr) {
		throw runtime_error(errorDecoding(ErrorCode::node_error));
	}

	return newNode;
}

void List::rangeCheck(size_t position, const ErrorCode& error) {
	if (position > size_) {
		throw runtime_error(errorDecoding(error)); // the error value is passed from the insert and erase functions
	}
}

void List::push_back(const TData data)
{
	auto newNode = newNodeCreation(data);

	if (empty())
	{
		head = tail = newNode;
	}
	else
	{
		auto oldTail = tail;
		tail = newNode;
		newNode->prev = oldTail;
		oldTail->next = newNode;
	}

	size_++;
}

void List::push_front(const TData data) {
	auto newNode = newNodeCreation(data);

	if (empty()) {
		head = newNode;
	}
	else {
		auto oldHead = head;
		head = newNode;
		newNode->next = oldHead;
		oldHead->prev = newNode;
	}

	size_++;
}

void List::pop_back() {
	if (empty()) {
		throw runtime_error(errorDecoding(ErrorCode::remove_error));
		return;
	}

	auto last_element = tail;
	tail = last_element->prev;
	tail->next = nullptr;
	delete last_element;

	size_--;
}

void List::pop_front() {
	if (empty()) {
		throw runtime_error(errorDecoding(ErrorCode::remove_error));
		return;
	}

	auto first_element = head;
	head = first_element->next;
	head->prev = nullptr;
	delete first_element;

	size_--;
}

void List::clear() {
	auto iter = head;

	while (iter)
	{
		auto next = iter->next;
		delete iter;
		iter = next;
	}

	head = tail = nullptr;

	size_ = 0;
}

size_t List::size() const {
	return size_;
}

optional<TData> List::at(size_t i) const {
	auto iter = head;
	size_t counter = 0;

	while (iter) {
		if (counter == i) {
			return make_optional(iter->data);
		}

		iter = iter->next;
		counter++;
	}

	return {};
}

void List::each(function<void(Node&)> callback) {
	auto iter = head;
	
	while (iter) {
		callback(*iter);
		iter = iter->next;
	}
}

void List::each(function<void(const Node&)> callback) const {
	auto iter = head;

	while (iter) {
		callback(*iter);
		iter = iter->next;
	}
}

void List::each(function<void(Node&, size_t)> callback) {
	auto iter = head;
	size_t counter = 0;

	while (iter) {
		callback(*iter, counter);
		iter = iter->next;
		counter++;
	}
}

void List::insertingBody(Node& iter, const TData& data, Node* newNode) {
	if (iter.prev == nullptr) {
		push_front(data);
	}
	else {
		auto pastNode = iter.prev;
		iter.prev = newNode;
		newNode->next = &iter;
		newNode->prev = pastNode;
		pastNode->next = newNode;
		size_++;
	}
}

void List::erasingBody(Node* iter) {
	if (iter->prev == nullptr) {
		pop_front();
		return;
	}
	else if (iter->next == nullptr) {
		pop_back();
		return;
	}
	else {
		auto next_element = iter->next;
		auto prev_element = iter->prev;
		next_element->prev = prev_element;
		prev_element->next = next_element;

		delete iter;
		size_--;
		return;
	}
}

void List::insert(size_t position, const TData& data) {
	auto newNode = newNodeCreation(data);
	size_t counter = 0;

	if (empty()) {
		head = tail = newNode;
	}
	else {
		rangeCheck(position, ErrorCode::insert_error);

		each([ & ](auto& iter, auto index) {
			if (index == position) {
				insertingBody(iter, data, newNode);
			}
		});
	}
}

void List::erase(size_t position) {
	auto iter = head;
	size_t counter = 0;

	if (empty()) {
		return;
	}
	else {
		rangeCheck(position, ErrorCode::erase_error);

		while (iter) {
			if (counter == position) {
				erasingBody(iter);
				return;
			}
			
			iter = iter->next;
			counter++;		
		}
	}
}

void List::merge(const List& object) {
	object.each([this](const Node& element) {
		push_back(element.data);
		});
}

List& List::operator=(List& object) {
	this->clear();

	object.each([&](Node& iter) {
		this->push_back(iter.data);
	});	

	return *this;
}

ostream& operator<<(ostream& cout, const optional<TData>& data) {
	if (data.has_value()) {
		cout << data.value();
	}
	else {
		cout << "No data";
	}

	return cout;
}

ostream& operator<<(ostream& cout, const List& object) {
	cout << "[ ";

	for (size_t i = 0; i < object.size(); i++) {
		cout << object.at(i);

		if (i != object.size() - 1u) {
			cout << ", ";
		}
	}

	cout << " ]";
	return cout;
}

void List::operator<<(const TData& a) {
	this->push_back(a);
}